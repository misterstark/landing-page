var clickSound = new Audio('/static/musics/SE_LoadingLogo.wav');
var playSound = function(audio) {
  audio.play();
  audio.volume = 0.2;
}
$('#btn-ok').click(function() {
  var isPlaying = clickSound.currentTime > 0 && !clickSound.paused
                  && !clickSound.ended && clickSound.readyState > 2;
  if (isPlaying) {
    clickSound.pause();
    clickSound.currentTime = 0;
    playSound(clickSound);
  }
  playSound(clickSound);
});

// Background Sound
/*
bgSound = new Audio('/static/musics/Atomos.mp3');
bgSound.addEventListener('ended', function() {
    this.currentTime = 0;
    playSound(this);
}, false);
playSound(bgSound);*/

var subscribtion_form = $('#subscribe');
var subscribtion_message = $('.input-subscribtion');
subscribtion_form.submit(function () {
  $.ajax({
      type: 'POST',
      url: '/subscribe/',
      data: subscribtion_form.serialize(),
      success: function (message) {
          subscribtion_message.css('color', '#f90');
          var html = '<div class="alert alert-special">'+message+'</div>';
          subscribtion_message.html(html);
      },
      error: function(message) {
          subscribtion_message.css('color', '#f90');
          subscribtion_message.html(message);
      }
  });
  return false;
});
