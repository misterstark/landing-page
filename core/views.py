from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from core.models import Subscription


def homepage(request):
    template_name = 'homepage.html'
    return render(request, template_name, {})


@csrf_exempt
def subscribe(request):
    if request.method == 'POST' and request.is_ajax():
        email = request.POST.get('emailadd')
        filter_email = Subscription.objects.filter(email=email)

        if email is None:
            return HttpResponse('Please enter your email to subscribe!')

        if filter_email.exists():
            return HttpResponse('Email %s already subscribed! Thank you.' % email)

        subs = Subscription.objects.create(email=email)
        subs.save()
        return HttpResponse('Thank you for your subscribtion.')
    return HttpResponse('Something went wrong!')
