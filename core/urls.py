from django.conf.urls import url

from core.views import *

urlpatterns = [
    url(r'^$', homepage, name='homepage'),
    url(r'^subscribe/$', subscribe, name='subscribe'),
]
