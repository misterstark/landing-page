from __future__ import absolute_import

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Subscription(models.Model):
    email = models.EmailField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Detial Subscription'
        verbose_name_plural = 'Subscriptions'
        ordering = ['-created']
