from django.contrib import admin

from core.models import Subscription


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ['email', 'created']
    search_fields = ['email']
    list_filter = ['created']
    list_per_page = 20

admin.site.register(Subscription, SubscriptionAdmin)
